package cn.itcast.itcaststore.web.filter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
/**
 * 编码过滤器（用于统一项目编码）
 */
public class EncodingFilter implements Filter {
	public void init(FilterConfig filterConfig) throws ServletException {

	}
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// 处理请求乱码		
		request.setCharacterEncoding("utf-8");
		// 处理响应乱码
		response.setContentType("text/html;charset=utf-8");
		chain.doFilter(request, response);
	}
	public void destroy() {

	}
}
